# frozen_string_literal: true

require 'erb'

PROTECT_GROUPS = [
  'Container Security'
].freeze

# add your groups here
SECURE_GROUPS = [
  'Composition Analysis',
  'Dynamic Analysis',
  'Fuzz Testing',
  'Static Analysis',
  'Threat Insights',
  'Vulnerability Research'
].freeze

ALL_GROUPS = SECURE_GROUPS + PROTECT_GROUPS

GROUPS_BY_STAGE = {
  'devops::protect' => PROTECT_GROUPS,
  'devops::secure' => SECURE_GROUPS
}.freeze

# adds a whitelisting function to strings (used for markdown link names)
class String
  def strip_special
    gsub(%r{[^0-9A-Za-z \\/:;,\-+]}, '')
  end
end

# some useful utilty functions
module Utils
  def self.compute_http_params(dict)
    dict.map { |k, v| "#{k}=#{v}" }.join('&')
  end

  def self.group_label(name)
    "group::#{ERB::Util.url_encode(name.downcase)}"
  end

  def self.group_selectors_by(teams:, labels: '')
    group_selectors_h = teams.map do |group_name|
      group_label = group_label(group_name)
      stage, = GROUPS_BY_STAGE.detect { |_stage, groups| groups.member?(group_name) }
      ["#{stage},#{group_label}" "#{',' unless labels.empty?}#{labels}", group_name]
    end.to_h

    if group_selectors_h.empty?
      # treat devops stage as a group, defaulting to `devops::secure`
      group_selectors_h.merge!(GROUPS_BY_STAGE.keys.last)
    else
      group_selectors_h
    end
  end

  def self.markdown_link(name, url)
    "[#{name.strip_special}](#{url})"
  end
end
